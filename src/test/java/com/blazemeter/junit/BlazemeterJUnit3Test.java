package com.blazemeter.junit;


import junit.framework.TestCase;
import org.apache.jmeter.protocol.java.sampler.JUnitSampler;
import org.apache.jorphan.logging.LoggingManager;
import org.apache.log.Logger;
import org.junit.Test;

public class BlazemeterJUnit3Test extends TestCase {

    private static final Logger log = LoggingManager.getLoggerForClass();


    public void testGetJMeterVariable() {
        JUnitSampler sampler = new JUnitSampler();
        String foo = sampler.getThreadContext().getVariables().get("foo");
        log.info("foo variable value is: " + foo);
    }


}
